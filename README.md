# OBS AutoTransition

Cet outil permet de commander le logiciel "OBS" pour automatiser le
redimensionnement de fenêtre suivant si elles sont active ou pas (focus)

## Prérequis

### Installation

Pour pouvoir utiliser cet outil, il vous faut le plugin OBS "web-socket" pour
mettre en place la communication entre le processus et le logiciel OBS

Veuillez suive les instructions [ici](https://github.com/obs-websocket/obs-websocket)

Une version de **JAVA 13 minimum** est requise

Vous devez être sous **Windows 10** obligatoirement,
la bibliothèque si vous voulez utiliser l'affichage du clavier
en raison des appels systèmes.

### Utilisation

Pour utiliser le jar, il vous faut le lancer via la ligne de commande
Il vous faut également un fichier de configuration "config.json"

```shell
java -jar .\OBSPlugin.jar
```

ou bien si vous voulez avoir plusieurs fichiers de configuration,
vous pouvez passer en paramètre le chemin vers un fichier json

```shell
java -jar .\OBSPlugin.jar --config <FilePath.json>

ex:

java -jar .\OBSPlugin.jar --config .\vscodeAndTerminal.json
```

### Activer l'affichage clavier
vous pouvez activer l'affichage d'une petite fenêtre avec les touches que vous tapez, en ajoutant l'argument --keyboard dans votre commande

```
java -jar OBSPlugin.jar {...} --keyboard
```

### Création de la configuration JSON

Le fichier json commence tout d'abord par un objet vide

```json
{}
```

Ensuite on ajoute un noeud (node) correspondant au nom de notre **Sources**

```json
{
  "<NomSource>": {}
}
```

Comme ceci
![MainNodeExample](./img/main_node.png)
Pour l'instant, il n'est pas possible de spécifié un membre
d'un groupe, ou bien de spécifié une scène spécifique.
seul le nom de la source est pris en compte

#### Par la suite on spécifie 3 valeurs à l'intérieur de ce noeud

```json
{
  "<NomSource>": {
    "alias": "<NomFenêtre>",
    "animation": "<NomAnimation>",
    "animationDuration": <TempsAnimationEnMillisecondes>,
  }
}
```

1. alias

"alias" permet de définir le nom de la fenêtre pour reconnaitre la fenêtre active

2. animation

"animation" définit le type d'animation à utiliser lors de la transition
Pour l'instant les animations implémentées sont :

- linear
- ease-in-out
- ease-in-out-quart

3. animationDuration

"animationDuration" donne la durée de la transition en milliseconde

#### Il ne nous manque plus que les réglagles des états "default" et "onFocus"

```json
{
  "<NomSource>": {
    "alias": "<NomFenêtre>",
    "animation": "<NomAnimation>",
    "animationDuration": <TempsAnimationEnMillisecondes>,
    "default": {
      // Les données sur la source ici
    },
    "onFocus": {
      // Les données sur la source ici
    }
  }
}
```

Comme sont nom l'indique, l'état "default" correspond à l'état par défaut de la fenêtre, lorsqu'elle n'est pas active.

_Pour récupérer les données d'une source sous format json:_

Lorsque le .jar est lancé, et qu'il est correctement connecté à OBS,
on peut taper la commande

```shell
.get <SourceName> --json
```

pour récuprer l'état de la source à savoir sa position, son rognement,
son redimensionnement etc...

Exemple:

```shell
.get VSCodeBlur_ --json
```

Retour:

```json
{
  "visible": true,
  "rotation": 0,
  "name": "VSCodeBlur_",
  "bounds": { "x": 0, "y": 0, "alignement": 0, "type": "OBS_BOUNDS_NONE" },
  "scale": { "x": 1, "y": 1 },
  "position": { "x": 14, "y": 105, "alignement": 0 },
  "crop": { "top": 0, "left": 0, "bottom": 0, "right": 964 }
}
```

Ces données sont ensuite ajouté au noeud "default", ce qui donne (par exemple)

```json
{
  "<NomSource>": {
    "alias": "<NomFenêtre>",
    "animation": "<NomAnimation>",
    "animationDuration": <TempsAnimationEnMillisecondes>,
    "default": {
      "visible": true,
      "rotation": 0,
      "name": "VSCodeBlur_",
      "bounds": { "x": 0, "y": 0, "alignement": 0, "type": "OBS_BOUNDS_NONE" },
      "scale": { "x": 1, "y": 1 },
      "position": { "x": 14, "y": 105, "alignement": 0 },
      "crop": { "top": 0, "left": 0, "bottom": 0, "right": 964 }
    },
    "onFocus": {
      // Les données sur la source ici
    }
  }
}
```

On redimensionne la fenêtre, puis on répète le processus pour mettre les données dans "onFocus".

#TODO Finir

---

## Prérequis à la compilation

#TODO



> par M. Fabien CAYRE