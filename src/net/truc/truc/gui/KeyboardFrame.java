package net.truc.truc.gui;

import static java.util.Map.entry;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.font.TextAttribute;
import java.text.AttributedString;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.stream.IntStream;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;

import lombok.Getter;
import lombok.Setter;
import net.truc.truc.utils.Pair;

public class KeyboardFrame extends JFrame {

  private static final double MINUTE = 60 * 1000;

  private TestStringComponent textDisplay;
  private Map<Integer, String> inputToDisplay = new HashMap<>();

  private KeyPerMinuteThread kpmThread;

  public KeyboardFrame() {
    super("KeyboardFrame");

    setSize(new Dimension(300, 200));

    setDefaultCloseOperation(EXIT_ON_CLOSE);
    setBackground(new Color(0, 255, 0));

    Font mainFont = new Font("Roboto Mono", Font.PLAIN, 54);
    Font fallbackFont = new Font("Serif", Font.PLAIN, 54);

    /*
     * JTextField textDisplay = new JTextField();
     * textDisplay.setBackground(Color.BLACK);
     * textDisplay.setForeground(Color.WHITE);
     * 
     * textDisplay.setFont(mainFont); textDisplay.setEditable(false); ceci uyn etst
     * textDisplay.setAlignmentX(CENTER_ALIGNMENT);
     * textDisplay.setAlignmentY(CENTER_ALIGNMENT);
     * un ^^peu ssu tous ça vous trouvez pas les gars ?????
     * 
     * 
     * 
     */
    TestStringComponent textDisplay = new TestStringComponent(mainFont, fallbackFont);

    textDisplay.setAlignmentX(CENTER_ALIGNMENT);
    textDisplay.setAlignmentY(CENTER_ALIGNMENT);
    textDisplay.setBackground(Color.BLACK);
    textDisplay.setForeground(Color.WHITE);
    Border border = textDisplay.getBorder();
    Border marginBorder = new EmptyBorder(new Insets(50, 0, 50, 0));
    textDisplay.setBorder(border == null ? marginBorder : new CompoundBorder(marginBorder, border));


    add(textDisplay);

    this.textDisplay = textDisplay;
    this.textDisplay.setText("       ");
    this.kpmThread = new KeyPerMinuteThread(this.textDisplay);
    this.kpmThread.start();

    setVisible(true);
    this.inputToDisplay = new HashMap<>();
    for (int i = 65; i < 90; i++) {
      this.inputToDisplay.put(i, String.valueOf((char) i).toLowerCase());
    }
    this.inputToDisplay.putAll(Map.ofEntries(entry(188, ","), entry(190, ";"), entry(191, ":"), entry(223, "!"),

        entry(222, "²"), entry(49, "&"), entry(50, "é"), entry(51, "\""), entry(52, "'"), entry(53, "("),
        entry(54, "-"), entry(55, "è"), entry(56, "_"), entry(57, "ç"), entry(48, "à"), entry(219, ")"),
        entry(187, "="),

        entry(13, "↩"), entry(8, "←"), entry(9, "↹"), entry(32, "⌴"), entry(226, "<"), entry(16, "⇧"),

        entry(37, "←"), entry(38, "↑"), entry(39, "→"), entry(40, "↓"), entry(17, "C^"),
        entry(91, "⌘")));
    for (int i = 0; i < 10; i++) {
      this.inputToDisplay.put(i + 96, String.valueOf(i));
    }
  }
  
  public void input(int input) {
    String s = this.inputToDisplay.get(input);
    if (s == null) {
      s = " ";
    }
    this.textDisplay.setText(this.textDisplay.getText().substring(s.length()) + s);
    this.kpmThread.appendTiming(System.currentTimeMillis());
    this.textDisplay.repaint();
  }

  class TestStringComponent extends JComponent {

    @Getter
    @Setter
    private String text;
    @Getter
    @Setter
    private String keyPerMinute;

    private Font mainFont;
    private Font fallBackFont;
    private Font subMainFont;

    public TestStringComponent(Font mainFont, Font fallBackFont) {
      this.text = "";
      this.keyPerMinute = "";
      this.mainFont = mainFont;
      this.fallBackFont = fallBackFont;

      this.subMainFont = this.mainFont.deriveFont(Font.ITALIC, 24);
    }

    @Override
    protected void paintComponent(Graphics g) {
      Graphics2D g2d = (Graphics2D) g;
      g.setColor(getBackground());
      g.fillRect(0, 0, getWidth(), getHeight());

      g.setColor(getForeground());
      g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
      g2d.drawString(createFallbackString(text, this.mainFont, this.fallBackFont).getIterator(), 20, 75);
      g2d.setFont(this.subMainFont);
      g2d.drawString(this.keyPerMinute, 20, 130);
    }

    public Dimension getPreferredSize() {
      return new Dimension(500, 40);
    }

    private AttributedString createFallbackString(String text, Font mainFont, Font fallbackFont) {
      AttributedString result = new AttributedString(text);

      int textLength = text.length();
      result.addAttribute(TextAttribute.FONT, mainFont, 0, textLength);

      for (int i = 0; i < text.length(); i++) {
        // On peut pas afficher ce caractère
        if (!mainFont.canDisplay(text.charAt(i))) {
          // On recherche jusqu'ou un peut pas afficher ce caractère
          int end = 0;
          for (int y = i; y < text.length(); y++) {
            // On peut afficher ce caractère alors on sort
            if (mainFont.canDisplay(text.charAt(y))) {
              break;
            }
            // On peut afficher ce caractère alors on boucle
            end = y;
          }
          end++;
          result.addAttribute(TextAttribute.FONT, fallbackFont, i, end);
          // le nombre de caractère qu'on a du fallback
          int fallbackCharCount = end - i;
          i += fallbackCharCount;
        }
      }

      return result;
    }
  }


  public class KeyPerMinuteThread extends Thread {

    private static final int MAX_SIZE = 25;
    private Vector<Pair<Long, Long>> timings = new Vector<>();
    private final TestStringComponent textDisplay;
    private long lastKey = -1;

    public KeyPerMinuteThread(TestStringComponent textDisplay2) {
      setDaemon(true);
      this.textDisplay = textDisplay2;
    }

    public void appendTiming(long timing) {
      if(lastKey == -1){
        lastKey = System.currentTimeMillis()-5;
      }
      long delta = timing - lastKey;
      this.lastKey = timing;
      this.timings.add(new Pair<>(timing, delta));
    }

    public double evaluateKeyPerMinute() {
      if (timings.isEmpty())
        return 0.0;

      double weightSum = IntStream.range(0, MAX_SIZE)
        .map(i -> i >= this.timings.size() ? 1 : 5)
        .sum();
      double averageTimingGap = IntStream.range(0, MAX_SIZE)
          // On somme les delta entre chaque valeurs
          .mapToDouble(i -> i >= this.timings.size() ? 100000 * 1 : this.timings.get(i).getSecond() * 5D)
          // On divise par le nombre d'élement dans la liste

          .sum() / weightSum;
      

      // Si la moyenne de différence est trop petite, on renvoie 0 
      if(averageTimingGap < 1.0){
        return 0;
      }
      // On calcule ensuite le nombre de touche par minute
      double keyPerMinute = MINUTE / averageTimingGap;
      //if(keyPerMinute > 1000) return 0; // Des fois monte a plus de 1000??? (bizarre)
      return keyPerMinute;
    }

    @Override
    public void run() {
      while (true) {
        try {
          long currentMillils = System.currentTimeMillis();
          
          List<Pair<Long, Long>> tempCopy = new ArrayList<>(this.timings);
          for (var timing : tempCopy) {
            if (currentMillils - 5_000 > timing.getFirst()) {
              timings.remove(timing);
            }
          }
          int kpm = (int) evaluateKeyPerMinute();
          SwingUtilities.invokeLater(() -> {
            this.textDisplay.setKeyPerMinute(String.valueOf(kpm) + " kpm");
            this.textDisplay.repaint();
          });
          
          Thread.sleep(50);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }

  }

}
