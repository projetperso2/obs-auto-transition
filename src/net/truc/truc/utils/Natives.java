package net.truc.truc.utils;

import com.sun.jna.Native;
import com.sun.jna.PointerType;
import com.sun.jna.Pointer;
import com.sun.jna.platform.unix.X11;

public class Natives {

	public static String getCurrentWindow() {
		switch (OsCheck.getOperatingSystemType()) {
			case Linux:
				final X11 x11 = X11.INSTANCE;
				final UserUnix xlib = UserUnix.INSTANCE;
				X11.Display display = x11.XOpenDisplay(null);
				X11.Window window = new X11.Window();
				xlib.XGetInputFocus(display, window, Pointer.NULL);
				X11.XTextProperty name = new X11.XTextProperty();
				x11.XGetWMName(display, window, name);
				return name.toString();
			case Windows:
				byte[] winText = new byte[512];
				PointerType hwnd = User32.INSTANCE.GetForegroundWindow();
				User32.INSTANCE.GetWindowTextA(hwnd, winText, 512);
				return Native.toString(winText);
			case MacOS:
			case Other:
			default:
				return "undefined";
		}
	}

}
