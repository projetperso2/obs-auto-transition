package net.truc.truc.utils;

import net.twasi.obsremotejava.objects.SceneProperties;

public class CustomSceneProperties {

	private SceneProperties defaultProperties;
	private SceneProperties onFocusProperties;
	private String windowName;
	private String sourceName;
	private String animation;
	private long animationDuration;
	
	public CustomSceneProperties(String sourceName, SceneProperties defaultProperties, SceneProperties onFocusProperties, String windowName) {
		this.defaultProperties = defaultProperties;
		this.onFocusProperties = onFocusProperties;
		this.windowName = windowName;
		this.sourceName = sourceName;
	}	
	
	public CustomSceneProperties(
		String key,
		SceneProperties defaultProps,
		SceneProperties onFocusProps,
		String windowName,
		String animation,
		long animationDuration) {
		this(key, defaultProps, onFocusProps, windowName);
		this.animation = animation;
		this.animationDuration = animationDuration;
	}

	public String getSourceName() {
		return this.sourceName;
	}
	
	public SceneProperties getDefaultProperties() {
		return this.defaultProperties;
	}
	
	public SceneProperties getOnFocusProperties() {
		return this.onFocusProperties;
	}
	
	public String getWindowName() {
		return this.windowName;
	}
	
	public String getAnimation() {
		return this.animation;
	}
	
	public long getAnimationDuration() {
		return this.animationDuration;
	}


	@Override
	public String toString() {
		return "{" +
			" defaultProperties='" + getDefaultProperties() + "'" +
			", onFocusProperties='" + getOnFocusProperties() + "'" +
			", windowName='" + getWindowName() + "'" +
			", sourceName='" + getSourceName() + "'" +
			"}";
	}

	
	
	
}
