package net.truc.truc.utils;

public class MathUtils {

	/**
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param x le nombre
	 * @param b La base du logarithme
	 * @return
	 * @date 23/12/2019
	 */
	public static int logB(
		int x,
		int b) {
		return (int) (Math.log(x) / Math.log(b));
	}

	/**
	 * Permet de transformer une valeur X d'un intervalle I[m, n] vers une valeur X'
	 * vers un intervalle I'[m', n']
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param x       La valeur à mappé
	 * @param in_min  le m de l'intervalle I
	 * @param in_max  le n de l'intervalle I
	 * @param out_min le m' de l'intervalle I'
	 * @param out_max le n' de l'intervalle I'
	 * @return la valeur X' étant dans l'intervalle I'[n', m']
	 * @date 21/12/2019
	 */
	public static float map(
		float x,
		float in_min,
		float in_max,
		float out_min,
		float out_max) {
		return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
	}
	
	/**
	 * Permet de transformer une valeur X d'un intervalle I[m, n] vers une valeur X'
	 * vers un intervalle I'[m', n']
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param x       La valeur à mappé
	 * @param in_min  le m de l'intervalle I
	 * @param in_max  le n de l'intervalle I
	 * @param out_min le m' de l'intervalle I'
	 * @param out_max le n' de l'intervalle I'
	 * @return la valeur X' étant dans l'intervalle I'[n', m']
	 * @date 21/12/2019
	 */
	public static double map(
		double x,
		double in_min,
		double in_max,
		double out_min,
		double out_max) {
		return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
	}

	/**
	 * Permet de mettre une valeur de N dans un intervalle I[n, m]
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param x   la valeur
	 * @param min la valeur n de l'intervalle I
	 * @param max la valeur m de l'intervalle I
	 * @return min si x < min <br>
	 *         x si min < x < max <br>
	 *         max si x > max <br>
	 * @date 21/12/2019
	 */
	public static int clamp(
		int x,
		int min,
		int max) {
		if (x < min) {
			x = min;
		} else if (x > max) {
			x = max;
		}
		return x;
	}

	/**
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param x
	 * @return
	 * @date 30/01/2020
	 */
	public static int floor(
		float x) {
		return (int) Math.floor(x);
	}

	/**
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param x
	 * @return
	 * @date 30/01/2020
	 */
	public static int floor(
		double x) {
		return (int) Math.floor(x);
	}

	/**
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param x
	 * @return
	 * @date 30/01/2020
	 */
	public static int ceil(
		float x) {
		return (int) Math.ceil(x);
	}

	/**
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param x
	 * @return
	 * @date 30/01/2020
	 */
	public static int ceil(
		double x) {
		return (int) Math.ceil(x);
	}

	public static final class Fast {
		private static final int BIG_ENOUGH_INT = 16 * 1024;
		private static final double BIG_ENOUGH_FLOOR = BIG_ENOUGH_INT;
		private static final double BIG_ENOUGH_ROUND = BIG_ENOUGH_INT + 0.5;

		public static int floor(
			float x) {
			return (int) (x + BIG_ENOUGH_FLOOR) - BIG_ENOUGH_INT;
		}

		public static int round(
			float x) {
			return (int) (x + BIG_ENOUGH_ROUND) - BIG_ENOUGH_INT;
		}

		public static int ceil(
			float x) {
			return BIG_ENOUGH_INT - (int) (BIG_ENOUGH_FLOOR - x); // credit: roquen
		}

	}

}
