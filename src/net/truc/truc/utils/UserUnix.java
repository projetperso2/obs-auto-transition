package net.truc.truc.utils;

import com.sun.jna.Pointer;
import com.sun.jna.Native;
import com.sun.jna.win32.StdCallLibrary;
import com.sun.jna.platform.unix.X11;

public interface UserUnix extends StdCallLibrary {
  
    
    UserUnix INSTANCE = (UserUnix) Native.loadLibrary("XLib", UserUnix.class);

    int XGetInputFocus(X11.Display display, X11.Window focus_return, Pointer revert_to_return);
    

}
