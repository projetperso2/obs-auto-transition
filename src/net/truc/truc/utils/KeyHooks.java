package net.truc.truc.utils;

import java.util.Map.Entry;

import lc.kra.system.keyboard.GlobalKeyboardHook;
import lc.kra.system.keyboard.event.GlobalKeyAdapter;
import lc.kra.system.keyboard.event.GlobalKeyEvent;
import net.truc.truc.Main;

public class KeyHooks extends Thread {

    private Main main;
    private GlobalKeyboardHook keyboardHook;
    private boolean run;



    public KeyHooks(Main main) {
        setDaemon(true);
        // Might throw a UnsatisfiedLinkError if the native library fails to load or a
        // RuntimeException if hooking fails
        this.keyboardHook = new GlobalKeyboardHook(true); // Use false here to switch to hook instead of raw input
        System.out.println(
                "Global keyboard hook successfully started, press [escape] key to shutdown. Connected keyboards:");
        for (Entry<Long, String> keyboard : GlobalKeyboardHook.listKeyboards().entrySet()) {
            System.out.format("%d: %s\n", keyboard.getKey(), keyboard.getValue());
        }
        this.main = main;
        this.keyboardHook.addKeyListener(new GlobalKeyAdapter() { 
            @Override
            public void keyPressed(GlobalKeyEvent event) {
                //System.out.println(event.getVirtualKeyCode());
            }
            
            @Override
            public void keyReleased(GlobalKeyEvent event) {
                KeyHooks.this.main.getKeyboardFrame().input(event.getVirtualKeyCode());
                //System.out.println(event.getVirtualKeyCode());
            }
        });
        this.run = true;

    }

    @Override
    public void run() {
        try {
            while (this.run) {
                Thread.sleep(10);
            }
        } catch (InterruptedException e) {
        } finally {
            keyboardHook.shutdownHook();
        }
    }

}