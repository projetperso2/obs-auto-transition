package net.truc.truc.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import com.google.gson.Gson;

import net.twasi.obsremotejava.objects.SceneProperties;

public class SourceConfig {
	
	private List<CustomSceneProperties> props;
	
	public SourceConfig(){
		this("./config.json");
	}

	public SourceConfig(String configName) {
		this.props = new ArrayList<CustomSceneProperties>();
		JSONObject config = loadConfig(configName);
		for(String key : config.keySet()) {
			JSONObject sourceConfig = config.getJSONObject(key);
			
			JSONObject defaultConfig = sourceConfig.getJSONObject("default");
			JSONObject onFocusConfig = sourceConfig.getJSONObject("onFocus");
			

			
			String windowName = sourceConfig.getString("alias");
			
			SceneProperties defaultProps = new Gson().fromJson(defaultConfig.toString(), SceneProperties.class);
			SceneProperties onFocusProps = new Gson().fromJson(onFocusConfig.toString(), SceneProperties.class);
			
			String animation = "none";
			if(sourceConfig.has("animation")) {
				animation = sourceConfig.getString("animation");
			}
			
			long animationDuration = 25;
			if(sourceConfig.has("animationDuration")) {
				animationDuration = sourceConfig.getLong("animationDuration");
			}
			
			this.props.add(new CustomSceneProperties(key, defaultProps, onFocusProps, windowName, animation, animationDuration));
		}
	}
	
	public List<CustomSceneProperties> getProps() {
		return this.props;
	}
	
	public JSONObject loadConfig(String filePath) {
		StringBuffer stringBuffer = new StringBuffer();
		File file = new File(filePath);
		if (!file.exists()) {
			return new JSONObject();
		}
		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String line = "";
			while ((line = reader.readLine()) != null) {
				stringBuffer.append(line);
			}
			reader.close();
			return new JSONObject(stringBuffer.toString());
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("Erreur");
		}
		return new JSONObject();
	}
	
}
