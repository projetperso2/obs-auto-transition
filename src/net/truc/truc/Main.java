package net.truc.truc;

import java.util.Arrays;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicBoolean;

import lombok.Getter;
import net.truc.truc.animation.Animation;
import net.truc.truc.animation.AnimationManager;
import net.truc.truc.animation.AnimationProcess;
import net.truc.truc.commands.CommandThread;
import net.truc.truc.gui.KeyboardFrame;
import net.truc.truc.utils.CC;
import net.truc.truc.utils.CustomSceneProperties;
import net.truc.truc.utils.KeyHooks;
import net.truc.truc.utils.Natives;
import net.truc.truc.utils.SourceConfig;
import net.twasi.obsremotejava.OBSRemoteController;
import net.twasi.obsremotejava.objects.Scene;
import net.twasi.obsremotejava.objects.Source;
import net.twasi.obsremotejava.requests.GetCurrentScene.GetCurrentSceneResponse;
import net.twasi.obsremotejava.requests.GetSceneList.GetSceneListResponse;

public class Main extends Thread {

	public static final long MAX_IDLE_TIMEOUT = 3_000_000;

	public static final String INFO = CC.YELLOW_BOLD + " ? " + CC.RESET + CC.WHITE_BOLD;
	public static final String WARN = CC.RED_BOLD + " ! " + CC.RESET + CC.WHITE_BOLD;
	public static final String FINE = CC.GREEN_BOLD + " > " + CC.RESET + CC.WHITE_BOLD;
	public static final String TAB = "\t";

	public static void main(String[] args) {

		OBSRemoteController controller = new OBSRemoteController("ws://localhost:4444", false);
		if (controller.isFailed()) { // Awaits response from OBS
			// Here you can handle a failed connection request
			System.out.println(WARN + "Connexion à OBS échouée.");
			System.exit(1);
		}

		controller.registerConnectCallback(res -> System.out.println(FINE + "Le système est connecté"));

		controller.registerOnError((msg, err) -> {
			err.printStackTrace();
			System.out.println(msg);
		});
		long maxIdle = controller.getClient().getMaxIdleTimeout();
		System.out.println(INFO + "Max Idle Timeout : " + CC.CYAN + (maxIdle / 1000) + CC.RESET + "s (" + maxIdle + "ms).");
		System.out.println(INFO + "Setting new max idle timeout ...");
		controller.getClient().setMaxIdleTimeout(1000 * MAX_IDLE_TIMEOUT);
		System.out.println(FINE + "Max idle timeout updated ...");

		maxIdle = controller.getClient().getMaxIdleTimeout();
		System.out.println(INFO + "Max Idle Timeout : " + CC.CYAN + (maxIdle / 1000) + CC.RESET + "s (" + maxIdle + "ms).");

		boolean debug = false;
		boolean keyBoardDisplay = false;
		String config = null;

		if (args.length >= 1) {
			List<String> argList = Arrays.asList(args);
			debug = argList.contains("--debug");

			int indexConfig = -1;
			if ((indexConfig = argList.indexOf("--config")) != -1) {
				try {
					config = argList.get(indexConfig + 1);
					System.out.println(FINE + "Fichier de config custom trouvé !");
					System.out.println(FINE + "Utilisation de '" + config + "'");
				} catch (Exception e) {
					System.out.println(WARN + "Merci de fournir un fichier valide.");
					System.out.println(INFO + "Utilisation de la config par défaut (config.json)");
				}
			}

			keyBoardDisplay = argList.contains("--keyboard");
		}
		SourceConfig sourceConfig = config == null ? new SourceConfig() : new SourceConfig(config);

		new Main(controller, sourceConfig.getProps(), debug, keyBoardDisplay).start();
	}

	private OBSRemoteController controller;
	private String currentWindow;
	private Scene currentScene;
	private boolean searchingScene;
	private List<CustomSceneProperties> list;
	private AnimationManager animation;
	private List<String> currentAnimation;
	private AtomicBoolean isShuttingDown;
	private CommandThread commandThread;
	private KeyHooks keyhookThread;
	@Getter
	private KeyboardFrame keyboardFrame;
	private final boolean debug;

	public Main(OBSRemoteController controller, List<CustomSceneProperties> list, boolean debug,
			boolean keyBoardDisplay) {
		this.debug = debug;
		this.controller = controller;
		this.currentWindow = "";
		this.searchingScene = false;
		this.list = list;
		this.isShuttingDown = new AtomicBoolean(false);

		controller.registerSwitchScenesCallback(res -> {
			this.currentScene = null;
			tryGetCurrentScene();
		});

		this.currentAnimation = new Vector<>();
		this.animation = new AnimationManager();
		this.commandThread = new CommandThread(this);

		if (keyBoardDisplay) {
			this.keyboardFrame = new KeyboardFrame();
			this.keyhookThread = new KeyHooks(this);
			this.keyhookThread.start();
		}

		controller.registerDisconnectCallback((res) -> {
			System.out.println(WARN + "Le web hook s'est déconnecté...");
		});

	}

	public void tryGetCurrentScene() {
		this.searchingScene = true;
		System.out.println(INFO + "Recherche de la scene courrante...");
		this.controller.getScenes(res -> {
			GetSceneListResponse response = (GetSceneListResponse) res;
			final List<Scene> scenes = response.getScenes();
			controller.getCurrentScene(res1 -> {
				GetCurrentSceneResponse response1 = (GetCurrentSceneResponse) res1;
				System.out.println(FINE + "La scene à été mise à jour (" + response1.getName() + ")");
				scenes.stream().filter(scene -> scene.getName().equals(response1.getName())).findFirst().ifPresent(scene -> {
					this.currentScene = scene;
				});
			});
		});
	}

	public boolean isSearchingScene() {
		return this.searchingScene;
	}

	public boolean hasCurrentScene() {
		return this.currentScene != null;
	}

	public List<Source> getSource() {
		return this.currentScene.getSources();
	}

	boolean windowNameChange = false;

	@Override
	public void run() {
		this.commandThread.start();
		while (true) {
			try {
				Thread.sleep(100);
				final String window = Natives.getCurrentWindow();
				final String oldWindow = this.currentWindow;

				if (!window.equals(this.currentWindow)) {
					this.currentWindow = window;
					this.windowNameChange = true;
				}
				if (!hasCurrentScene()) {
					if (!isSearchingScene()) {
						tryGetCurrentScene();
					}
					continue;
				}

				if (windowNameChange) {

					CustomSceneProperties oldProps = findProps(oldWindow);
					CustomSceneProperties newProps = findProps(window);

					if (oldProps == newProps) {
						// SAME WINDOWS so no real windows changes
						this.windowNameChange = false;
						continue;
					}

					System.out.println(INFO + "Window name changed");

					System.out.println(INFO + " Old windows : '" + oldWindow + "'");
					System.out.println(INFO + " New windows : '" + window + "'");

					if (oldProps != null) {
						System.out.println(INFO + "Old properties found");
						if (this.debug) {
							System.out.println(INFO + "Props for '" + oldWindow + "' ");
							System.out.println(oldProps);
						}
						if (!isAnimate(oldProps.getSourceName())) {
							Animation animation = this.animation.getAnimation(oldProps.getAnimation());
							if (animation != Animation.NONE) {
								System.out.println(TAB + INFO + "Starting transition thread...");
								new AnimationProcess(this, animation, oldProps, oldProps.getOnFocusProperties(),
										oldProps.getDefaultProperties()).start();
							} else {
								this.controller.setSceneProperties(this.getCurrentScene().getName(), oldProps.getSourceName(),
										oldProps.getDefaultProperties(), r -> {
										});
							}
						} else {
							System.out.println(TAB + WARN + "Transition already found for old props, aborting...");
							continue;
						}
					}

					if (newProps != null) {
						System.out.println(INFO + "New properties found");
						if (this.debug) {
							System.out.println(INFO + "Props for '" + window + "' ");
							System.out.println(newProps);
						}
						if (!isAnimate(newProps.getSourceName())) {
							Animation animation = this.animation.getAnimation(newProps.getAnimation());
							if (animation != Animation.NONE) {
								System.out.println(TAB + INFO + "Starting transition thread...");
								new AnimationProcess(this, animation, newProps, newProps.getDefaultProperties(),
										newProps.getOnFocusProperties()).start();
							} else {
								this.controller.setSceneProperties(this.getCurrentScene().getName(), newProps.getSourceName(),
										newProps.getOnFocusProperties(), r -> {
										});
							}
						} else {
							System.out.println(TAB + WARN + "Transition already found for new props");
						}
					}
				}
				this.windowNameChange = false;
			} catch (InterruptedException e) {
				e.printStackTrace();
				this.controller.disconnect();
			}
		}
	}

	private CustomSceneProperties findProps(String windowName) {
		for (CustomSceneProperties props : this.list) {
			if (windowName.contains(props.getWindowName())) {
				return props;
			}
		}
		return null;
	}

	public Scene getCurrentScene() {
		return this.currentScene;
	}

	public OBSRemoteController getController() {
		return this.controller;
	}

	public AnimationManager getAnimation() {
		return this.animation;
	}

	public boolean isAnimate(String sourceName) {
		return this.currentAnimation.contains(sourceName);
	}

	public void startAnimation(String sourceName) {
		System.out.println(TAB + Main.INFO + " Début d'animation sur la source " + sourceName + ".");
		this.currentAnimation.add(sourceName);
	}

	public void endAnimation(String sourceName) {
		System.out.println(TAB + Main.INFO + " Fin d'animation sur la source " + sourceName + ".");
		this.currentAnimation.remove(sourceName);
	}

}
