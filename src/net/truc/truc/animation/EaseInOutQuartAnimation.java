package net.truc.truc.animation;

public class EaseInOutQuartAnimation extends SingleFunctionAnimation{

  public EaseInOutQuartAnimation() {
    super("ease-in-out-quart");
  }

  @Override
  protected double app(double x) {
    return x < 0.5 ? 8 * x * x * x * x : 1 - Math.pow(-2 * x + 2, 4) / 2;
  }


}
