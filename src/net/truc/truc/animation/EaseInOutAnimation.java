package net.truc.truc.animation;

public class EaseInOutAnimation extends SingleFunctionAnimation {

	public EaseInOutAnimation() {
		super("ease-in-out");
	}

	/**
	 * 
	 * Bezier
	 * T entre 0 et 1
	 * Le truc est entre 0 et 1
	 * Donc en gros c'est comme si on faisait de 0% a 100%
	 * @author Fabien CAYRE (Computer)
	 *
	 * @param t
	 * @return
	 * @date 17/11/2020
	 */
	@Override
	protected double app(double t) {
		return t * t * (3.0f - 2.0f * t);
	}

}
