package net.truc.truc.animation;

import net.truc.truc.utils.MathUtils;

public class LinearAnimation extends Animation {

	public LinearAnimation() {
		super("linear");
	}

	@Override
	public int getX(AnimationData data, long delta) {
		return (int) MathUtils.map(delta, 1, data.endTime, data.startX, data.endX);
	}

	@Override
	public int getY(AnimationData data, long delta) {
		return (int) MathUtils.map(delta, 0, data.endTime, data.startY, data.endY);
	}

	@Override
	public double getScaleX(AnimationData data, long delta) {
		return MathUtils.map(delta, 0D, data.endTime, data.startScaleX, data.endScaleX);
	}
	
	@Override
	public double getScaleZ(AnimationData data, long delta) {
		return MathUtils.map(delta, 0D, data.endTime, data.startScaleY, data.endScaleY);
	}

	@Override
	public int getCropTop(AnimationData data, long delta) {
		return  (int) MathUtils.map(delta, 0, data.endTime, data.startCropTop, data.endCropTop);
	}

	@Override
	public int getCropBottom(AnimationData data, long delta) {
		return  (int) MathUtils.map(delta, 0, data.endTime, data.startCropBottom, data.endCropBottom);
	}

	@Override
	public int getCropLeft(AnimationData data, long delta) {
		return  (int) MathUtils.map(delta, 0, data.endTime, data.startCropLeft, data.endCropLeft);
	}

	@Override
	public int getCropRight(AnimationData data, long delta) {
		return  (int) MathUtils.map(delta, 0, data.endTime, data.startCropRight, data.endCropRight);
	}


}
