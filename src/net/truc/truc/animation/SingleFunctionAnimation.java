package net.truc.truc.animation;

import net.truc.truc.utils.MathUtils;

public abstract class SingleFunctionAnimation extends Animation{

  public SingleFunctionAnimation(String name) {
    super(name);
  }

  /**
	 * 
	 * Bezier
	 * T entre 0 et 1
	 * Le truc est entre 0 et 1
	 * Donc en gros c'est comme si on faisait de 0% a 100%
	 * @author Fabien CAYRE (Computer)
	 *
	 * @param t
	 * @return
	 * @date 17/11/2020
	 */
	protected abstract double app(double t);
	
	/**
	 * 
	 * @author Fabien CAYRE (Computer)
	 *
	 * @param delta
	 * @return
	 * @date 17/11/2020
	 */
	private double perc(AnimationData data, long delta) {
		return Double.valueOf(delta) / Double.valueOf(data.endTime);
	}
	
	@Override
	public int getX(AnimationData data, long delta) {
		return (int) MathUtils.map(app(perc(data, delta)), 0, 1.0, data.startX, data.endX);
	}

	@Override
	public int getY(AnimationData data, long delta) {
		return (int) MathUtils.map(app(perc(data, delta)), 0, 1.0, data.startY, data.endY);
	}

	@Override
	public double getScaleX(AnimationData data, long delta) {
		return MathUtils.map(app(perc(data, delta)), 0, 1.0, data.startScaleX, data.endScaleX);
	}

	@Override
	public double getScaleZ(AnimationData data, long delta) {
		return MathUtils.map(app(perc(data, delta)), 0, 1.0, data.startScaleY, data.endScaleY);
	}

	@Override
	public int getCropTop(AnimationData data, long delta) {
		return (int) MathUtils.map(app(perc(data, delta)), 0, 1.0, data.startCropTop, data.endCropTop);
	}

	@Override
	public int getCropBottom(AnimationData data, long delta) {
		return (int) MathUtils.map(app(perc(data, delta)), 0, 1.0, data.startCropBottom, data.endCropBottom);
	}

	@Override
	public int getCropLeft(AnimationData data, long delta) {
		return (int) MathUtils.map(app(perc(data, delta)), 0, 1.0, data.startCropLeft, data.endCropLeft);
	}

	@Override
	public int getCropRight(AnimationData data, long delta) {
		return (int) MathUtils.map(app(perc(data, delta)), 0, 1.0, data.startCropRight, data.endCropRight);
	}


}
