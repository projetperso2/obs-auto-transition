package net.truc.truc.animation;

public abstract class Animation {

	public static final Animation NONE = null;

	private String name;

	public Animation(String name) {
		this.name = name;
	}

	public abstract int getX(AnimationData data, long delta);

	public abstract int getY(AnimationData data, long delta);

	public abstract double getScaleX(AnimationData data, long delta);

	public abstract double getScaleZ(AnimationData data, long delta);

	public abstract int getCropTop(AnimationData data, long delta);

	public abstract int getCropBottom(AnimationData data, long delta);

	public abstract int getCropLeft(AnimationData data, long delta);

	public abstract int getCropRight(AnimationData data, long delta);



	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}

	public String toString(AnimationData data, long delta) {
		return String
			.format("{x:%d, y:%d, scale_x:%f, scale_y:%f}", getX(data, delta), getY(data, delta), getScaleX(data, delta), getScaleZ(data, delta));
	}

}
