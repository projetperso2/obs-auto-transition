package net.truc.truc.animation;

import net.truc.truc.Main;
import net.truc.truc.utils.CustomSceneProperties;
import net.twasi.obsremotejava.objects.SceneProperties;
import net.twasi.obsremotejava.objects.SceneProperties.Bounds;
import net.twasi.obsremotejava.objects.SceneProperties.Crop;
import net.twasi.obsremotejava.objects.SceneProperties.Position;
import net.twasi.obsremotejava.objects.SceneProperties.Scale;

public class AnimationProcess extends Thread {

	public static final long THREAD_SLEEP = 5;

	private Main main;
	private Animation animation;
	private CustomSceneProperties props;

	private long start;
	private SceneProperties buffer;

	private SceneProperties from;
	private SceneProperties to;
	private AnimationData animationData;

	public AnimationProcess(Main main, Animation animation, CustomSceneProperties props, SceneProperties from, SceneProperties to) {
		setDaemon(true);
		this.main = main;
		this.animation = animation;
		this.props = props;
		this.start = System.currentTimeMillis();
		this.from = from;
		this.to = to;

		this.buffer = new SceneProperties(
			this.props.getSourceName(),
			new Position(),
			0D,
			new Scale(),
			new Crop(),
			true,
			new Bounds());
		
		AnimationData data = new AnimationData();
		data.startX = from.getPosition().getX();
		data.startY = from.getPosition().getY();
		data.endX = to.getPosition().getX();
		data.endY = to.getPosition().getY();
		data.startScaleX = from.getScale().getX();
		data.startScaleY = from.getScale().getY();
		data.endScaleX = to.getScale().getX();
		data.endScaleY = to.getScale().getY();
		data.endTime = props.getAnimationDuration();

		data.startCropTop = from.getCrop().getTop();
		data.startCropLeft = from.getCrop().getLeft();
		data.startCropRight = from.getCrop().getRight();
		data.startCropBottom = from.getCrop().getBottom();
		

		data.endCropTop = to.getCrop().getTop();
		data.endCropLeft = to.getCrop().getLeft();
		data.endCropRight = to.getCrop().getRight();
		data.endCropBottom = to.getCrop().getBottom();
		
		this.animationData = data;
	}

	@Override
	public void run() {
		this.main.startAnimation(this.props.getSourceName());
		try {
			while (true) {
				long current = System.currentTimeMillis();
				long delta = current - this.start;
				if (delta > this.animationData.endTime) {
					// Stop the Thread
					this.main.endAnimation(this.props.getSourceName());
					break;
				}

				this.buffer.getPosition().setX(this.animation.getX(this.animationData, delta));
				this.buffer.getPosition().setY(this.animation.getY(this.animationData, delta));
				

				this.buffer.getScale().setX(this.animation.getScaleX(this.animationData, delta));
				this.buffer.getScale().setY(this.animation.getScaleZ(this.animationData, delta));

				this.buffer.getCrop().setBottom(this.animation.getCropBottom(this.animationData, delta));
				this.buffer.getCrop().setTop(this.animation.getCropTop(this.animationData, delta));
				this.buffer.getCrop().setRight(this.animation.getCropRight(this.animationData, delta));
				this.buffer.getCrop().setLeft(this.animation.getCropLeft(this.animationData, delta));
				

				this.main
					.getController()
					.setSceneProperties(
						this.main.getCurrentScene().getName(),
						this.props.getSourceName(),
						this.buffer,
						r -> {});
				
				Thread.sleep(THREAD_SLEEP);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
