package net.truc.truc.animation;

import java.util.HashMap;
import java.util.Map;

public class AnimationManager {

	private Map<String, Animation> animations;
	
	public AnimationManager() {
		this.animations = new HashMap<>();

		register(Animation.NONE, new LinearAnimation(), new EaseInOutAnimation(), new EaseInOutQuartAnimation());
	}


	public void register(Animation...animations){
		for(Animation animation : animations){
			if(animation == Animation.NONE){
				this.animations.put("none", animation);
			}else{
				this.animations.put(animation.getName(), animation);
			}
		}
	}
	
	public Map<String, Animation> getAnimations() {
		return this.animations;
	}

	public Animation getAnimation(String animation) {
		return this.animations.get(animation);
	}
	
}
