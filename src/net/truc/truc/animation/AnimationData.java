package net.truc.truc.animation;

public class AnimationData {

	public double startX;
	public double startY;
	public double startScaleX;
	public double startScaleY;

	public double endX;
	public double endY;
	public double endScaleX;
	public double endScaleY;
	
	public long endTime;
	public int startCropTop;
	public int startCropLeft;
	public int startCropRight;
	public int startCropBottom;
	public int endCropTop;
	public int endCropLeft;
	public int endCropRight;
	public int endCropBottom;
	
}
