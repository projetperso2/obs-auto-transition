package net.truc.truc.commands;

import java.util.Scanner;

import net.truc.truc.Main;

public class CommandThread extends Thread{

	private Main main;
	private CommandManager commandManager;
	
	public CommandThread(Main main) {
		this.main = main;
		setDaemon(true);
		this.commandManager = new CommandManager();
		this.commandManager.register(new GetCommand(this.main));
	}

	@Override
	public void run() {
		Scanner scanner = new Scanner(System.in);
		while(true) {
			try{
				if(scanner.hasNextLine()) {
					String input = scanner.nextLine();
					this.commandManager.handle(input);
				}
			}catch(Exception e){
				e.printStackTrace();
				break;
			}
		}
		scanner.close();
	}
	
}
