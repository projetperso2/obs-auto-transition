package net.truc.truc.commands;

import net.truc.truc.Main;

public class HelpCommand extends Command{
  
  private CommandManager commandManager;

  public HelpCommand (CommandManager commandManager){
    super("help", "Display list of all commands", "help");
    this.commandManager = commandManager;
  }

  @Override
  public boolean handle(String[] args) {
    System.out.println(Main.INFO+" Command list :");
    for(Command command : this.commandManager.getCommands().values()){
      System.out.println(Main.TAB+"."+command.getName()+" : "+command.getDesc()+" (Usage: '."+command.getUsage()+"')");
    } 
    return true;
  }

}
