package net.truc.truc.commands;

import java.util.HashMap;
import java.util.Map;

import lombok.Getter;
import net.truc.truc.Main;
import net.truc.truc.utils.CC;

public class CommandManager {

  @Getter
  private Map<String, Command> commands;

  public CommandManager (){
    this.commands = new HashMap<>();
    register(new HelpCommand(this));
  }

  public void register(Command...commands){
    for(Command c : commands){
      this.commands.put(c.getName(), c);
    }
  }

  public void handle(String input){
    if(input.startsWith(".")){
      // Input start with "." so it's a command

      // Split all input by spaces
      String[] fullCommand = input.substring(1).split("\\s+");
      
      if(fullCommand.length > 0){
        // Command array contains more than 0 element
        String commandName = fullCommand[0];
        Command command = null;
        if( (command = this.commands.get(commandName)) != null ){
          // Command exist
          String[] arguments = new String[fullCommand.length - 1];
          System.arraycopy(fullCommand, 1, arguments, 0, fullCommand.length - 1);
          if(!command.handle(arguments)){
            System.out.println(Main.WARN+CC.RED_BRIGHT+"."+command.getUsage()+CC.RESET);
          }
        }
      }
    }
  }
  
}
