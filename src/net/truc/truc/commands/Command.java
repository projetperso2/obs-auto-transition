package net.truc.truc.commands;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public abstract class Command {
  
  @Getter
  @Setter
  private String name;

  @Getter
  @Setter
  private String desc;

  @Getter
  @Setter
  private String usage;


  public abstract boolean handle(String[] args);


}
