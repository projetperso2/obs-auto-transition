package net.truc.truc.commands;

import org.json.JSONObject;

import net.truc.truc.Main;
import net.twasi.obsremotejava.objects.Source;
import net.twasi.obsremotejava.requests.GetSceneItemProperties.GetSceneItemPropertiesResponse;

public class GetCommand extends Command {

  private Main main;

  public GetCommand(Main main) {
    super("get", "Display sources data on selected scene", "get [SourceName (--json)] ");
    this.main = main;
  }

  @Override
  public boolean handle(String[] args) {
    if (args.length == 0) {
      main.getSource().forEach(source -> {

        main.getController().getSceneItemProperties(main.getCurrentScene().getName(), source.getName(), res -> {
          GetSceneItemPropertiesResponse response = (GetSceneItemPropertiesResponse) res;
          System.out.println(Main.INFO + "Nom de la source : " + response.getName());
          System.out.println(Main.TAB + "Bottom crop de la source : " + response.getCrop().getBottom());
          System.out.println(Main.TAB + "Top crop de la source : " + response.getCrop().getTop());
          System.out.println(Main.TAB + "Right crop de la source : " + response.getCrop().getRight());
          System.out.println(Main.TAB + "Left crop de la source : " + response.getCrop().getLeft());
          System.out.println(Main.TAB + "Position X la source : " + response.getPosition().getX());
          System.out.println(Main.TAB + "Position Y de la source : " + response.getPosition().getY());
          System.out.println(Main.TAB + "Bound X de la source : " + response.getBounds().getX());
          System.out.println(Main.TAB + "Bound Y de la source : " + response.getBounds().getY());
          System.out.println(Main.TAB + "Scale X de la source : " + response.getScale().getX());
          System.out.println(Main.TAB + "Scale Y de la source : " + response.getScale().getY());
        });
        System.out.println();
      });
      return true;
    } else {
      boolean json = false;
      // check if last args is json
      if(args[args.length - 1].equals("--json")){
        json = true;
      }
      
      StringBuilder strBuilder = new StringBuilder();
      
      // If it's json, we don't take the last argument as sourceName
      for(int i = 0; i < (json ? args.length - 1 : args.length); i++){
        strBuilder.append(args[i]+" ");
      }
      String sourceName = strBuilder.replace(strBuilder.length()  - 1 , strBuilder.length(), "").toString();
      Source source = main.getSource().stream()
            .filter(s -> s.getName().equals(sourceName)).findFirst().orElse(null);
      if(source == null){
        return false;
      }
      final boolean isJson = json;
      main.getController().getSceneItemProperties(main.getCurrentScene().getName(), source.getName(), res -> {
        GetSceneItemPropertiesResponse response = (GetSceneItemPropertiesResponse) res;
        if(isJson){
          System.out.println(JSONObject.wrap(response.asSceneProperties()).toString());
        }else{
          System.out.println(Main.INFO + "Nom de la source : " + response.getName());
          System.out.println(Main.TAB + "Bottom crop de la source : " + response.getCrop().getBottom());
          System.out.println(Main.TAB + "Top crop de la source : " + response.getCrop().getTop());
          System.out.println(Main.TAB + "Right crop de la source : " + response.getCrop().getRight());
          System.out.println(Main.TAB + "Left crop de la source : " + response.getCrop().getLeft());
          System.out.println(Main.TAB + "Position X la source : " + response.getPosition().getX());
          System.out.println(Main.TAB + "Position Y de la source : " + response.getPosition().getY());
          System.out.println(Main.TAB + "Bound X de la source : " + response.getBounds().getX());
          System.out.println(Main.TAB + "Bound Y de la source : " + response.getBounds().getY());
          System.out.println(Main.TAB + "Scale X de la source : " + response.getScale().getX());
          System.out.println(Main.TAB + "Scale Y de la source : " + response.getScale().getY());
        }
      });
      System.out.println();
      return true;
    }
    //return false;
  }

}
